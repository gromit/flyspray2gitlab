import gitlab
import os

from tqdm import tqdm


def main():
    gl = gitlab.Gitlab('https://gitlab.archlinux.org',
                       private_token=os.environ['GITLAB_TOKEN'])

    # group_name = "archlinux/packaging/packages"
    group_name = "flyspray-migration/packaging/packages"
    group = gl.groups.get(group_name)
    projects = group.projects.list(get_all=True)
    # projects = group.projects.list()

    for project in projects:
        print(project.path)


if __name__ == "__main__":
    main()
