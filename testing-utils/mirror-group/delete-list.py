import gitlab
import os

from tqdm import tqdm


def main():
    gl = gitlab.Gitlab('https://gitlab.archlinux.org',
                       private_token=os.environ['GITLAB_TOKEN'])

    id_list = [
        68940,
        68381,
        64424,
        61904,
        63218,
        61607,
        61586,
        61472,
        61218,
        60905,
        59490,
        59487,
        59188,
        59190,
        59195,
        58658,
        58346,
    ]

    for project_id in id_list:
        project = gl.projects.get(project_id)
        print(project.path_with_namespace)
        # gl.projects.delete(project.id)
        gl.projects.delete(project.id, permanently_remove=True, full_path=project.path_with_namespace)


if __name__ == "__main__":
    main()
