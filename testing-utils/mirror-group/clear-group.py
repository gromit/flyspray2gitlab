import gitlab
import os

from tqdm import tqdm


def main():
    gl = gitlab.Gitlab('https://gitlab.archlinux.org',
                       private_token=os.environ['GITLAB_TOKEN'])

    target_group_name = "flyspray-migration/packaging/packages"
    target_group = gl.groups.get(target_group_name)
    target_projects = target_group.projects.list(get_all=True)
    # target_projects = target_group.projects.list()

    for project in tqdm(target_projects):
        # print(project.path_with_namespace)
        gl.projects.delete(project.id, full_path=project.path_with_namespace)
        gl.projects.delete(project.id, permanently_remove=True, full_path=project.path_with_namespace)


if __name__ == "__main__":
    main()
