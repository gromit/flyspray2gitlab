import gitlab
import os

from tqdm import tqdm


def main():
    gl = gitlab.Gitlab('https://gitlab.archlinux.org',
                       private_token=os.environ['GITLAB_TOKEN'])

    source_group_name = "archlinux"
    source_group = gl.groups.get(source_group_name)
    source_labels = source_group.labels.list(get_all=True)
    # source_projects = source_group.projects.list()

    target_group_name = "flyspray-migration"
    target_group = gl.groups.get(target_group_name)

    # for project in tqdm(target_projects):
    #     gl.projects.delete(project.id)

    for label in source_labels:
        print(label)
        try:
            target_group.labels.create(
                {
                "name": label.name,
                "color": label.color,
                "description": label.description,
                })
        except gitlab.exceptions.GitlabCreateError as error:
            target_group.labels.update(
                label.name, {
                "new_name": label.name,
                "color": label.color,
                "description": label.description,
                })


if __name__ == "__main__":
    main()
