import gitlab
import os

from tqdm import tqdm


def main():
    gl = gitlab.Gitlab('https://gitlab.archlinux.org',
                       private_token=os.environ['GITLAB_TOKEN'])

    source_group_name = "archlinux/packaging/packages"
    source_group = gl.groups.get(source_group_name)
    source_projects = source_group.projects.list(get_all=True)
    # source_projects = source_group.projects.list()

    target_group_name = "flyspray-migration/packaging/packages"
    target_group = gl.groups.get(target_group_name)
    # target_projects = target_group.projects.list(get_all=True)

    # for project in tqdm(target_projects):
    #     gl.projects.delete(project.id)

    for project in tqdm(source_projects):
        try:
            gl.projects.create({
                'path': project.path,
                'namespace_id': target_group.id,
                'visibility': 'public'
            }, )
        except gitlab.exceptions.GitlabCreateError as error:
            pass


if __name__ == "__main__":
    main()
