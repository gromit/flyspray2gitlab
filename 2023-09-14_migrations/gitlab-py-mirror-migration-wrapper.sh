#!/bin/bash - 

set -o nounset                              # Treat unset variables as an error

# used to test migrating the mirror issues :)

GITLAB_TOKEN="$(gopass show --password bugmigration-test)" ../gitlab.py \
    --skip-attachments \
    --attachments "." \
    --upstream "https://bugs.archlinux.org" \
    --base "https://gitlab.archlinux.org" \
    --default-target "gromit/testing-please-ignore" \
    --default-namespace "flyspray-migration" \
    --id-mapping-output "id-mapping-mirrors.json" \
    --summary-mapping-output "summary-mapping-mirrors.json" \
    --limit-category "Mirrors" \
    --dry-mappings \
    --verbose \
    --dump-file ../flyspray-archlinux-project.json \
    --project-mapping ./project-mapping.json \
    --category-mapping ./category-mapping.json \
    import
