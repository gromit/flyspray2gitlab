#!/bin/bash - 

set -o nounset                              # Treat unset variables as an error

# used to migrate the namcap issues

GITLAB_TOKEN="..." ../gitlab.py \
    --skip-attachments \
    --attachments "." \
    --upstream "https://bugs.archlinux.org" \
    --base "https://gitlab.archlinux.org" \
    --default-target "gromit/testing-please-ignore" \
    --default-namespace "flyspray-migration" \
    --id-mapping-output "id-mapping-arch_install_scripts.json" \
    --summary-mapping-output "summary-mapping-arch_install_scripts.json" \
    --filter-summary "arch-install-scripts" \
    --verbose \
    --dry-mappings \
    --dump-file ../flyspray-all-projects.json \
    --project-mapping ./project-mapping.json \
    import
