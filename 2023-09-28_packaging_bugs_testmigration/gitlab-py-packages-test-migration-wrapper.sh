#!/bin/bash - 

set -o nounset                              # Treat unset variables as an error

# used to test the migration of the packaging issues

GITLAB_TOKEN="..." ../gitlab.py \
    --skip-attachments \
    --attachments "." \
    --upstream "https://bugs.archlinux.org" \
    --base "https://gitlab.archlinux.org" \
    --default-target "flyspray-migration/packaging/bug-repo" \
    --default-namespace "flyspray-migration/packaging/packages" \
    --id-mapping-output "id-mapping-testmigration.json" \
    --summary-mapping-output "summary-mapping-testmigration.json" \
    --verbose \
    --dry-mappings \
    --dump-file ../flyspray-archlinux-project.json \
    import
